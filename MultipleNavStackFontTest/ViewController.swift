//
//  ViewController.swift
//  MultipleNavStackFontTest
//
//  Created by Tommie N. Carter, Jr., MBA on 11/18/15.
//  Copyright © 2015 MING Technology. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //log the font names available before adding them to the plist file
        for family: String in UIFont.familyNames()
        {
            print("\(family)")
            for names: String in UIFont.fontNamesForFamilyName(family)
            {
                print("== \(names)")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func unwindToMainViewController (segue:UIStoryboardSegue){
        
    }

}

